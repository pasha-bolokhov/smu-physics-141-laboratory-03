%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}
\renewcommand{\theequation}{\arabic{equation}}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory III  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Free Fall}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics I --- Physics 141L/171L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	The purpose of this laboratory is to investigate the acceleration of gravity
	and compare your results to the generally accepted properties of gravity

\bigskip\noindent
	The equation of motion for a body starting \emph{from rest} and undergoing constant acceleration
	can be expressed as
\ccc
\beq
\label{x}
	x	~~=~~	\frac 1 2\, a\, t^2\
	\ccb\,,\ccc
\eeq
\ccb
	where \cc{ x } is the distance the object has traveled from its starting point,
	\cc{ a } is the acceleration, and \cc{ t } is the time elapsed since the motion began

\bigskip\noindent
	In order to measure the acceleration caused by gravity, several questions must be considered:
\begin{itemize}
\item
	Is the acceleration constant?
	If it is, then the distance an object falls will be proportional to the square of the elapsed time,
	as in the above equation

\item
	If the acceleration is constant, what is the value of the acceleration?
	Is it the same for all objects or does it vary with mass or size of the object, or with some other quality of the object?
	If it is not constant, how does it vary?
\end{itemize}

\bigskip\noindent
	In this experiment you will answer some of these questions by carefully timing the fall of a sensor from various heights


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                             P R E P A R A T I O N                            %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Preparation}}

\begin{center}
\includegraphics[width=4cm]{pasco-ps-3202-force-acceleration-sensor.jpg}
\end{center}

\noindent
	Please note that although PASCO PS--3202 sensor does measure acceleration, we will not be relying on the values it provides.
	Instead, we will be relying on the \emph{timing} of events it will report

\begin{itemize}
\item
	Attach a clamp to a lab stand or any other device that will hold it vertical
	and at the desired height over the floor or table.
	\emph{Set your equipment for fall distances of approximately
	\cc{ \SI 2 \metre }, \cc{ \SI{1.5}\metre }, \cc{ \SI 1 \metre }, \cc{ \SI{0.5}\metre }, \cc{ \SI{0.25}\metre }
	and other heights as desired}

\item
	Position the receptor basket directly under the sensor
\end{itemize}

\noindent
	On your computer or phone
\begin{itemize}
\item
	Start \textsl{SPARKvue} app

\item
	Connect the app to the sensor using Bluetooth

\item
	Hit \textsl{Start New Experiment} $ \rightarrow $ \textsl{Build New Experiment}

\item
	Choose a double--pad layout so you can see a graph and data simultaneously
\raisebox{-2.4mm}{
\begin{tikzpicture}
	% white background
	\draw	[draw = cyan, fill = white, very thin]
		(-0.7, -0.4)	rectangle	(0.7, 0.4);

	% left pad
	\draw	[draw = none, fill = pastelblue]
		(-0.6, -0.3)	rectangle	(0.2, 0.3);

	% right pad
	\draw	[draw = none, fill = pastelblue]
		(0.3, -0.3)	rectangle	(0.6, 0.3);
\end{tikzpicture}
}

\item
	Select a graph option in the left pad. 
	Hit \textsl{Select Measurement} and choose
	\textsl{Acceleration(Resultant)} in the list of sensors

\item
	Select a table option in the right pad.
	Choose \textsl{Time} and \textsl{Acceleration(Resultant)}
	for the two columns of data.
	This way you have both the graphical and data representation
	of the timing

\item
	Change the sampling rate to \cc{ \SI{200}\hertz }

\item
	Make sure to \emph{zero the sensor} before each measurement
\end{itemize}

\noindent
	You are now ready to take data.
	At any moment, you can erase the data runs that you have collected


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         D A T A   C O L L E C T I O N                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Data collection}}

\noindent
	For each of the heights indicated above, do \emph{five} measurements of timing.
	Make sure that you have measured the actual drop height the sensor is going to fly freely
\begin{itemize}
\item
	Position the sensor at the required height

\item
	Zero the sensor.
	The sensor must be at rest for this

\item
	Press \textsl{Start}

\item
	When you see the data points starting to run, release the sensor

\item
	\textsl{Stop} data collection right after the sensor lands --- you do not need too much unwanted data
\end{itemize}

\noindent
	You can save or export your data runs.
	Ultimately you are interested in the flight time of the sensor.
	Look for where the \emph{flight plateau} starts and ends in time.
	The graph will help you identify these times roughly,
	while the data table will tell you the times more precisely.
	The time length of the flight is the ultimate result of each measurement.
	However, you need to average a few measurements as indicated above.
	The table below provides one possible format for your data recording.
	\emph{Make sure to include the units}

\begin{center}
\begin{tabular}{
	>{\centering\arraybackslash}m{2cm}
	>{\centering\arraybackslash}m{2cm}
	>{\centering\arraybackslash}m{2cm}
	>{\centering\arraybackslash}m{2cm}
	>{\centering\arraybackslash}m{2cm}
	>{\centering\arraybackslash}m{2cm} |
	>{\centering\arraybackslash}m{2cm}
}
%
	\textbf{Distance}	&	\cc{ \mathbold{t_1} }	&	\cc{ \mathbold{t_2} }
				&	\cc{ \mathbold{t_3} }	&	\cc{ \mathbold{t_4} }	
				&	\cc{ \mathbold{t_5} }	&	\cc{ \mathbold{t_\text{avg} } }	\\[2mm]
%
\toprule
\noalign{\vskip 2mm}
%
	\\[2mm]
\hline
\noalign{\vskip 2mm}
%
	\\[2mm]
\hline
\noalign{\vskip 2mm}
%
	\\[2mm]
\hline
\noalign{\vskip 2mm}
%
	\\[2mm]
\hline
\noalign{\vskip 2mm}
%
	\\[2mm]
\bottomrule
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

	Using Google Sheets, plot a graph of the
	distance (\cc{ d }) versus \cc{ t_\text{avg} } with \cc{ d } as the dependent value (\cc{ y }--axis).
	Within the limits of your experimental accuracy,
	do your data points define a straight line or a curve?
	Is there a significant difference in the shape of the graph?
	What basic relationship does the graph appear to resemble?

\bigskip\noindent
	Now, again using Google Sheets, plot a graph of \cc{ d } versus \cc{ t_\text{avg}^2 }
	with \cc{ d } as the dependent value (\cc{ y }--axis).
	Within the limits of your experimental accuracy, do your data points now define a straight line?
	Was the acceleration constant?

\bigskip\noindent
	If your graphs were linear, use Google Sheets to add a trend--line (slope) to your linear graphs.
	What does this slope represent?
	What would it represent if you plotted double the distance 
	of fall on the vertical axis (\emph{i.e.} \cc{ 2\,d } versus \cc{ t_\text{avg} }),
	or \cc{ \frac 1 2\,t_\text{avg}^2 } on the horizontal axis (\emph{i.e.} \cc{ d } versus \cc{ \frac 1 2\, t_\text{avg}^2 })?
	Using your calculated slope(s) and equation \eqref{x} shown in the introduction to this experiment,
	determine the acceleration caused by gravity.
	\emph{Be sure to include the units}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                          E R R O R   A N A L Y S I S                         %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Error analysis}}

	The expected (allowable) error for this experiment is dependent on the measurement uncertainties for the height
	of the drop and the time it takes to fall.
	This can be represented as,
\ccc
\beq
\label{error}
	e_a	~~=~~	\frac{\Delta a} a \times 100\%
	\ccb\,,
	\qquad\qquad\qquad\qquad
	\ccc
	\frac{\Delta a} a	~~=~~
	\frac {\Delta x} x  ~+~  \frac {2\, \Delta t} t
	\ccb\,,\ccc
\eeq
\ccb
	where,

\begin{tabular}{l c l}
	\cc{ e_a }	&	--- 	&	is the resulting error
			(percentage uncertainty in the calculated value of acceleration)\\[1mm]
	\cc{ \Delta a\,/\, a }
			&	---	&	is the relative uncertainty of the acceleration\\[1mm]
	\cc{ \Delta x }	&	---	&	is the uncertainty in the measurement of the height of the drop\\[1mm]
	\cc{ x }	&	---	&	is the measured distance the sensor dropped\\[1mm]
	\cc{ \Delta t }	&	---	&	is the uncertainty in the measurement of the time for the sensor to drop\\[1mm]
	\cc{ t }	&	---	&	is the average time that the sensor took to fall from each height
\end{tabular}

\bigskip\noindent
	First you are going to calculate the quantity \cc{ \frac{\Delta a} a } using the rightmost equation \eqref{error} above.
	From this you can find \cc{ \Delta a}, although this is not required in this laboratory.
	Using the ratio \cc{ \frac{\Delta a} a } you find the percentage expected error \cc{ e_a }.

\bigskip\noindent
	The experimental (observed) error is the calculated value of acceleration compared to an accepted standard.
	This can be represented as,
\ccc
\[
	\frac{ a ~-~ g } g \,\times\, 100\%	~~=~~	\%\, \text{observed error}
	\ccb\,,
\]
\ccb
	where,

\begin{tabular}{l c l}
	\cc{ a }	&	---	&	is the calculated value of the acceleration of gravity\\[1mm]
	\cc{ g }	&	---	&	is the accepted value of the acceleration of gravity (\cc{ \SI{9.81}{\metre\per\second\squared} })
\end{tabular}

\bigskip\noindent
	The meaning of the \emph{expected} error is the \emph{uncertainty} due to your measurements.
	The meaning of the \emph{observed} error is how far your actual result \cc{ a } is from the predicted value \cc{ g }.
	If your experiment is to agree with the prediction, which of the errors should be larger?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                              C O N C L U S I O N                             %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Conclusion}}

	Describe your laboratory experiment and discuss your results.
	Consider answering the following question
\begin{itemize}
\item
	is the acceleration caused by gravity constant?
\end{itemize}

\noindent
	Discuss the conditions under which you believe your results to be true.
	Include a discussion of the errors in your measurements and how they affect your conclusions


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
